// Library to do times communication with the WS2812 backed LEDs
#include <Adafruit_NeoPixel.h>

// Web server for ESP
#include <ESP8266WebServer.h>
#include "htmlpage.h"

// default BAUD rate for adruino IDE's COM3 console
#define BAUD 74880

// PIN 5 = GPIO5 = D1
#define PIN 5

// CJMCU-2812-8 : strip of 8 WS2812 RGB LEDs
#define LED_COUNT 8

// Instance of the Adafruit_NeoPixel
// WS2812 LEDs expect 3 bytes to identify colors as green-red-blue (NOT RGB!) at 800 kHz
Adafruit_NeoPixel leds = Adafruit_NeoPixel(LED_COUNT, PIN, NEO_GRB + NEO_KHZ800);

// web server
ESP8266WebServer server(80);

// LED mode
int mode = 2;

// state of a program
int step = 0;

// custom RGB value
int led_red = 0;
int led_green = 0;
int led_blue = 0;

// brightness of LEDs
// 0 = off, 255 = max, 64 = 1/4 brightness
int brightness = 64;

void sendPage() {
  int inMode = server.arg("mode").toInt();
  int inBright = server.arg("bright").toInt();
  int inRed = server.arg("red").toInt();
  int inGreen = server.arg("green").toInt();
  int inBlue = server.arg("blue").toInt();
  if (inRed > 0 || inGreen > 0 || inBlue > 0) {
    mode = 1;
    led_red = inRed & 255;
    led_green = inGreen & 255;
    led_blue = inBlue & 255;
    Serial.print("Setting RGB = ");
    Serial.print(led_red);
    Serial.print("/");
    Serial.print(led_green);
    Serial.print("/");
    Serial.println(led_blue);

  } else if (inMode > 0 && inMode != mode) {
    step = -1;
    mode = inMode;
    Serial.print("Mode changed to ");
    Serial.println(mode);

  } else if (inBright > 0 && inBright != brightness) {
    switch (inBright) {
      case 1:
        brightness = 32;
        break;
      case 2:
      default:
        brightness = 64;
        break;
      case 3:
        brightness = 128;
        break;
      case 4:
        brightness = 255;
        break;
    }
    Serial.print("Brightness changed to ");
    Serial.println(brightness);
  }

  server.send(200, "text/html", web_page);
}

void setup() {
  // start debugging output
  Serial.begin(BAUD);
  Serial.println("Starting");

  WiFi.mode(WIFI_STA); // client
  const char* ssid     = "UPSnet";
  const char* password = "Executor";
  WiFi.begin(ssid, password);

  // Wait until WiFi is connected
  Serial.print("Connecting to WiFI ");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected.");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // start server
  server.onNotFound(sendPage);
  server.begin();
  Serial.println("HTTP server started");

  // start neo pixel
  leds.begin();

  // print configuration
  Serial.print("Output pin: GPIO");
  Serial.println(PIN);
  Serial.print("Number of LEDs: ");
  Serial.println(LED_COUNT);
  Serial.print("Brightness (0..255): ");
  Serial.println(brightness);
}

void loop() {
  switch (mode) {
    case 1:
      for (int i = 0; i < leds.numPixels(); i++) {
        leds.setPixelColor(i, led_red, led_green, led_blue);
      }
      leds.show();
      break;
    case 2:
    default:
      mode = 2;
      rainbow();
      break;
    case 3:
      rainbowCycle();
      break;
    case 4:
      rgbChase();
      break;
    case 5:
      flashWhite();
      break;
  }
  server.handleClient();
  delay(50);
}

// example from neo pixel lib
void rainbow() {
  step = (step + 1) % 256;
  for (int i = 0; i < leds.numPixels(); i++) {
    leds.setPixelColor(i, Wheel((i + step) & 255));
  }
  leds.setBrightness(brightness);
  leds.show();
}

// example from neo pixel lib
// Make the rainbow equally distributed throughout the strip
void rainbowCycle() {
  step = (step + 1) % (256 * 5);
  for (int i = 0; i < leds.numPixels(); i++) {
    leds.setPixelColor(i, Wheel(((i * 256 / leds.numPixels()) + step) & 255));
  }
  leds.setBrightness(brightness);
  leds.show();
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return leds.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if (WheelPos < 170) {
    WheelPos -= 85;
    return leds.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return leds.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void flashWhite() {
  for (int i  = 0; i < leds.numPixels(); i++) {
    leds.setPixelColor(i, 255, 255, 255);
  }
  leds.setBrightness(brightness);
  leds.show();
}

void rgbChase() {
  step = (step + 1) % 96;

  uint32_t c;
  if (step < 16) c = leds.Color(step * 16, 0, 0);
  else if (step < 32) c = leds.Color(511 - step * 16, 0, 0);
  else if (step < 48) c = leds.Color(0, step * 16, 0);
  else if (step < 64) c = leds.Color(0, 1023 - step * 16, 0);
  else if (step < 80) c = leds.Color(0, 0, step * 16);
  else c = leds.Color(0, 0, 1535 - step * 16);

  for (int i  = 0; i < leds.numPixels(); i++) {
    leds.setPixelColor(i, c);
  }
  leds.setBrightness(brightness);
  leds.show();
}

