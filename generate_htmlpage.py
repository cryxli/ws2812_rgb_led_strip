#!/usr/bin/python
# -------------------------------------------------------
# This script turns a HTML file into a C header constant
# which can be imported and used in Arduino.
# -------------------------------------------------------

import sys;

# Send escaped file's content to STDOUT.
def convert_html(filename):
    f = open(filename, 'r');
    for line in f:
        s = line.rstrip();
        t = s.lstrip();
        if t[:2] == '//' or t == '':
            continue;
        # escape backslash and double-quotes
        sys.stdout.write(s.strip().replace('\\','\\\\').replace('"', '\\"'));
    f.close();

# Print usage info
def print_help(cmd):
    print("python "+cmd+" [variable name] <HTML file>");

# Parse arguments and filter input file
def analyse_arguments(args):
    if len(args) < 2:
        print_help(args[0])
        return
    variable = 'web_page';
    if len(args)-2 > 0:
        variable = args[len(args)-2];
    sys.stdout.write('\n');
    sys.stdout.write('const char* ');
    sys.stdout.write(variable);
    sys.stdout.write(' = "');
    convert_html(args[len(args)-1]);
    sys.stdout.write('";\n');

# Entry point
if __name__ == '__main__':
    analyse_arguments(sys.argv)

