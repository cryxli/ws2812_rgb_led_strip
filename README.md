## Drive CJMCU-2812-8 with an ESP8266 using Arduino IDE

This project is about how to program a ESP8266 to control a strip of WS2812 RGB LEDs using Arduino IDE.


### CJMCU-2812-8

![CJMCU-2812-8](CJMCU-2812-8.jpg)

The CJMCU-2812-8 simply is a pre-assembled board with eight 5050 RGB LEDs that have a WS2812 chip to control the colors. It uses two separate circuits:

- ``4-7VDC`` and adjacent ``GND`` to power the LEDs 
- ``DIN`` and ajdacent ``GND`` to send commands to the chips

Note the ``DOUT`` that indicates that multuple boards can be chained together.

Possible source is/was [Banggood](https://www.banggood.com/CJMCU-8-Bit-WS2812-5050-RGB-LED-Driver-Development-Board-p-958214.html?p=6712164627281201605T).


### NodeMCU ESP8266 dev kit

![NodeMCU ESP8266](NodeMCU.jpg)

> NodeMCU is an open source IoT platform. It includes firmware which runs on the [ESP8266 Wi-Fi SoC from Espressif Systems](https://espressif.com/en/products/hardware/esp8266ex/overview), and hardware which is based on the ESP-12 module. - [Wikipedia](https://en.wikipedia.org/wiki/NodeMCU)

The board extends the core ESP8266 chip which already has built in WiFI support with an USB programming/flashing interface and some ready to use pins. Basically turning the ESP8266 into an Arduino like platform for your projects.

The neat thing about ESP8266 is the possibility to run a web server on it and, therefore, remotely control it over WiFi.

Possible source is/was [Banggood](https://www.banggood.com/NodeMcu-Lua-WIFI-Internet-Things-Development-Board-Based-ESP8266-CP2102-Wireless-Module-p-1097112.html?p=6712164627281201605T).


### Arduino IDE

The [Arduino IDE](https://www.arduino.cc/en/Main/Software) does not support the NodeMCU dev kit by default. You have to install the required drivers first. It also does not support the WS2812 out of the box, unless you want to send and time data manuelly. Therefore, an appropriate library is required, too.

To install the ESP8266 driver go to ``File`` > ``Preferences`` and a the bottom under "Additional Board Managers", enter: 

    http://arduino.esp8266.com/versions/2.0.0/package_esp8266com_index.json 

To install the WS2812 library go to ``Sketch`` > ``Include Library`` > ``Manage Libraries...`` and search for "neopixel". Then install NeoPixel, not NeoMatrix.


### Wiring

By default the ESP8266 and WS2812 should be wired as follows:

| ESP8266            | WS2812     |
|--------------------|------------|
| ``GND``        	 | ``GND``    |
| ``5V``         	 | ``4-7VDC`` |
| ``D1`` / ``GPIO5`` | ``DIN``    |
| ``GND``            | ``GND``    |

Note that the LED board expects two ``GND``s and the ESP readily offers four ``GND``s. This makes sense since power and data can come from different sources each requiring a ``GND``.

Also the ``DIN`` pin can be configured in the code to connect to a different GPIO.
